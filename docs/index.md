# 기계 학습의 Bias-Variance 트레이드오프 <sup>[1](#footnote_1)</sup>

> <font size="3">bias-variance 트레이드오프가 무엇인지, 머신러닝 모델에 어떤 영향을 미치는지 알아봅니다. 알아본다.</font>

## 목차

1. [개요](./bias-variance-tradeoff.md#intro)
1. [bias-variance이란?](./bias-variance-tradeoff.md#sec_02)
1. [bias-variance 트레이드오프란?](./bias-variance-tradeoff.md#sec_03)
1. [bias-variance을 측정하는 방법](./bias-variance-tradeoff.md#sec_04)
1. [bias-variance을 줄이는 방법](./bias-variance-tradeoff.md#sec_05)
1. [마치며](./bias-variance-tradeoff.md#summary)

<a name="footnote_1">1</a>: [ML Tutorial 20 — Bias-Variance Tradeoff in Machine Learning](https://levelup.gitconnected.com/ml-tutorial-20-bias-variance-tradeoff-in-machine-learning-07eef1e6c195?sk=f0ef4aebc65083e70f02181c6be2f640)를 편역한 것이다.
