# 기계 학습의 Bias-Variance 트레이드오프

## <a name="intro"></a> 개요
이 포스팅에서는 기계 학습에서 가장 중요한 개념 중 하나인 bias-variance 트레이드오프에 대해 설명한다. bias와 variance이 무엇인지, 기계 학습 모델의 성능에 어떤 영향을 미치는지, 최적의 결과를 얻기 위해 이들의 균형을 맞추는 방법을 보일 것이다.

bias-variance 트레이드오프는 모든 기계 학습 실무자가 모델을 구축하고 평가할 때 직면하는 근본적인 딜레마이다. 이는 모델의 복잡성과 정확성 사이의 트레이드오프를 의미한다. 너무 단순한 모형은 높은 편향과 낮은 분산을 가질 수 있는데, 이는 데이터에 잘 맞지 않고 부정확한 예측을 한다는 것을 의미한다. 너무 복잡한 모델은 낮은 편향과 높은 분산을 가질 수 있는데, 이는 데이터에 너무 적합시켜 일관성 없는 예측을 한다는 것을 의미한다. 낮은 편향과 낮은 분산을 갖는 모형을 찾는 것이 목표이며, 이는 데이터에 잘 맞고 정확하고 신뢰할 수 있는 예측을 한다는 것을 의미한다.

이 목표를 달성하기 위해서는 모델의 편향과 분산을 측정하고 이를 줄이기 위한 다양한 기법을 사용해야 한다. 이 포스팅에서는 Python과 일부 인기 있는 기계 학습 라이브러리를 사용하여 모델을 수행하는 방법을 보일 것이다. 선형 회귀, 로지스틱 회귀, 의사결정 트리와 같은 bias-variance 트레이드오프가 다양한 유형의 기계 학습 모델에 어떤 영향을 미치는지에 대한 예도 들을 것이다.

이 포스팅에 대한 학습을 통하여 다음 작업을 수행할 수 있다.

- 편향과 분산이란, 기계 학습 모델에 미치는 영향
- 평균 제곱 오차, 정확도, 정밀도, 리콜 및 F1-score와 같은 메트릭을 사용한 모델의 편향과 분산 측정
- 교차 검증, 정규화, 피처 선택 및 앙상블 방법과 같은 기법을 사용하여 모델의 편향과 분산의 감소
- bias-variance 트레이드오프 개념을 다양한 타입의 기계 학습 모델과 문제에 적용

시작하기 전, Python과 기계 학습에 대한 지식이 어느 정도 있어야 한다. 다음 모듈 또는 라이브러리도 임포트해야 한다.

```python
# Import the libraries
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.metrics import mean_squared_error, accuracy_score, precision_score, recall_score, f1_score
from sklearn.linear_model import LinearRegression, LogisticRegression, Ridge, Lasso
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, VotingClassifier
```

bias-variance 트레이드오프에 대해 배울 준비가 되었나요? 시작해 보자!

## <a name="sec_02"></a> bias-variance이란?
편향과 분산은 기계 학습 모델의 품질을 측정하는 두 가지 개념이다. 이들은 모델이 주어진 입력의 출력을 예측할 때 발생하는 오차와 관련이 있다. 오차는 줄일 수 없는 오차, 편향, 분산의 세 구성요소로 나눌 수 있다.

줄일 수 없는 오차는 데이터에 내재된 무작위성이나 잡음 때문에 어떤 모델로도 제거할 수 없는 오차를 말한다. Bayes 오차라고도 하며, 어떤 모델도 달성할 수 있는 가능한 가장 낮은 오차를 뜻한다. 줄일 수 없는 오차는 보통 알 수 없으며 데이터 분포에 따라 달라진다.

편향은 모델이 참이 아니거나 정확하지 않은 가정을 했을 때 발생하는 오차이다. 그것은 모델에 대한 예상 예측과 실제 산출과의 차이이다. 편향은 모델이 정답으로부터 평균적으로 얼마나 떨어져 있는지를 측정한다. 편향이 높은 모델은 지나치게 단순화되어 데이터의 복잡성을 포착하지 못한다. 훈련 오차는 낮지만 검정 오차가 높아 데이터에 잘 맞지 않는 경향이 있다.

분산은 모델이 데이터의 작은 변화에 민감할 때 발생하는 오차이다. 주어진 입력에 대한 모델 예측의 변동성이다. 분산은 모형델 서로 다른 훈련 집합을 기반으로 예측을 얼마나 변화시키는지를 측정한다. 분산이 높은 모형은 과도하게 복잡하여 데이터의 잡음을 포착한다. 훈련 오차는 높지만 검정 오차가 낮은 경향이 있어 데이터에 과도하게 적합한 것으로 나타난다.

다음 그림은 샘플 데이터세트에서 서로 다른 모델의 편향과 분산을 보여준다. 검은 점은 실제 데이터 포인트이고, 색깔이 있는 선은 모델 예측이다. 파란색 선은 선형 모델이고, 초록색 선은 다항식 모델이고, 빨간색 선은 복합 모델이다.

> <span style="color:red">**그림과 설명이 불일치**</span>

![](./0_mHRbCm2ZIWeS6GMa.webp)

보다시피 선형 모델은 자료를 잘 맞추기에는 너무 단순하여 편향이 높고 분산이 낮다. 복합 모델은 자료를 너무 유연하게 잘 맞추어서 편향이 낮고 분산이 높다. 다항식 모형은 단순성과 복잡성의 균형이 잘 잡혀서 편향이 중간 정도이고 분산도 중간 정도이다.

기계 학습의 목표는 새로운 데이터에 대한 정확하고 일관된 예측을 하기 때문에 낮은 편향과 낮은 분산을 동시에 갖는 모델을 찾는 것이다. 그러나 이는 편향과 분산 사이에 상충관계(tradeoff)가 존재하기 때문에 달성하기가 쉽지 않다. 이 상충관계는 다음 절의 주제이다.

## <a name="sec_03"></a> bias-variance 트레이드오프란?
bias-variance 트레이드오프는 기계 학습 모델의 총 오차를 최소화하려고 할 때 발생하는 현상이다. 총 오차는 줄일 수 없는 오차, 편향, 분산의 합이다. 모델의 복잡성을 증가시킬수록 편향은 감소하지만 분산은 증가한다. 모델의 복잡성을 감소시킬수록 편향은 증가하지만 분산은 감소한다. 트레이드오프는 총 오차를 최소화하는 최적점을 찾는 것이다.

다음 그림은 bias-variance 트레이드오프를 그래프로 나타낸 것이다. x축은 모델 복잡도를 나타내고, y축은 오차를 나타낸다. 파란색 곡선은 편향, 녹색 곡선은 분산, 빨간색 곡선은 총 오차이다. 검은색 점은 총 오차가 최소가 되는 최적점이다.

![](./0_pUQiAzGUiDu7qAQe.jpeg)

모델 복잡도가 증가할수록 편향이 감소하고 분산이 증가한다. 모형 복잡도가 감소할수록 편의가 증가하고 분산이 감소한다. 최적점은 총오차가 가장 낮은 곳으로 편의와 분산의 균형을 이루는 곳이다.

왜 이런 트레이드오프가 일어날까? 직관적으로 더 복잡한 모델이 훈련 데이터에 더 잘 맞지만, 노이즈와 데이터에 관련 없는 세부 사항을 포착할 수도 있다는 것이다. 이것은 모델을 데이터의 작은 변화에 더 민감하게 만들고, 새로운 데이터에 대한 일반화 가능성을 줄인다. 덜 복잡한 모델은 훈련 데이터에 더 잘 맞지만, 또한 노이즈와 데이터의 관련 없는 세부 사항을 무시할 수도 있다. 이는 모델을 데이터의 작은 변화에 더 강건하게 만들고, 새로운 데이터에 더 일반화할 수 있게 한다.

어떻게 트레이드오프의 최적점을 찾을 수 있을까? 데이터, 모델, 문제에 따라 다르므로 확정적인 답은 없다. 그러나 모델의 편향과 분산을 측정하고 줄이는 데 도움이 되는 기법들이 있으며, 이에 대해서는 다음 절에서 논의할 것이다.

## <a name="sec_04"></a> bias-variance을 측정하는 방법
기계 학습 모델의 편향과 분산을 측정하기 위해서는 오차와 모형의 정확성을 정량화할 수 있는 측정지표(metric)들을 사용해야 한다. 회귀분석과 분류 등 여러 타입의 문제에 대해 서로 다른 측정지표가 존재한다. 이 절에서는 가장 일반적인 측정지표들과 Python에서 사용하는 방법을 소개하고자 한다.

출력이 연속형 값인 회귀 문제의 경우 가장 널리 사용되는 메트릭 중 하나는 평균 제곱 오차(MSE)이다. MSE는 실제 출력과 예측된 출력 사이의 평균 제곱 차이를 측정한다. MSE가 낮다는 것은 오차가 낮고 정확도가 높다는 것을 의미한다. MSE는 `sklearn.metrics` 모듈의 `mean_squared_error` 함수를 사용하여 계산할 수 있다. 예를 들어, 몇 가지 특징을 기반으로 주택 가격을 예측하는 선형 회귀 모형이 있다고 가정하자. 다음과 같이 훈련과 테스트 세트에 대한 모델의 MSE를 계산할 수 있다.

```python
# Import the libraries
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error


# Load the data
df = pd.read_csv("house_prices.csv")

# Split the data into features and target
X = df.drop("price", axis=1)
y = df["price"]
# Split the data into training and test sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Create and fit the model
model = LinearRegression()
model.fit(X_train, y_train)

# Predict the output on the training and test sets
y_train_pred = model.predict(X_train)
y_test_pred = model.predict(X_test)

# Calculate the MSE on the training and test sets
mse_train = mean_squared_error(y_train, y_train_pred)
mse_test = mean_squared_error(y_test, y_test_pred)
# Print the results
print("MSE on training set:", mse_train)
print("MSE on test set:", mse_test)
```

위의 코드 출력은 아래와 같다.

```
MSE on training set: 0.043
MSE on test set: 0.047
```

보다시피 훈련 세트의 MSE가 테스트 세트의 MSE보다 약간 낮으며, 이는 모델이 편향이 약간 더 높고 분산이 약간 더 낮음을 의미한다. 이는 모델이 훈련 세트에서 훈련되어 테스트 세트에 잘 일반화되지 않을 수 있기 때문에 예상되는 것이다. 그러나 MSE 간의 차이가 그리 크지 않으며, 이는 모델이 데이터를 너무 과적합하거나 과소적합하지 않음을 의미한다.

출력이 이산 값인 분류 문제의 경우, 가장 널리 사용되는 메트릭 중 하나는 정확도(accuracy)이다. 정확도는 전체 예측 중 올바른 예측의 비율을 측정한다. 정확도가 높다는 것은 오류가 적고 정확도가 높다는 것을 의미한다. 정확도는 `sklean.metrics` 모듈의 `accuracy_score` 함수를 사용하여 계산할 수 있다. 예를 들어, 일부 피처 기반으로 당뇨병 여부를 예측하는 로지스틱 회귀 모형이 있다고 가정하자. 다음과 같이 훈련과 테스트 세트에서 모델의 정확도를 계산할 수 있다.

```python
# Import the libraries
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score

# Load the data
df = pd.read_csv("diabetes.csv")

# Split the data into features and target
X = df.drop("diabetes", axis=1)
y = df["diabetes"]
# Split the data into training and test sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Create and fit the model
model = LogisticRegression()
model.fit(X_train, y_train)
# Predict the output on the training and test sets
y_train_pred = model.predict(X_train)
y_test_pred = model.predict(X_test)

# Calculate the accuracy on the training and test sets
acc_train = accuracy_score(y_train, y_train_pred)
acc_test = accuracy_score(y_test, y_test_pred)
# Print the results
print("Accuracy on training set:", acc_train)
print("Accuracy on test set:", acc_test)
```

위의 코드 출력은 아래와 같다.

```
Accuracy on training set: 0.782
Accuracy on test set: 0.792
```

보다시피 훈련 세트의 정확도가 테스트 세트의 정확도보다 약간 낮으며, 이는 모델이 편향이 약간 낮고 분산이 약간 높다는 것을 의미한다. 이는 모델이 훈련 세트에서 훈련을 받았기 때문에 테스트 세트보다 더 잘 수행해야 하지만 예상치 못한 결과를 얻었다. 그러나 정확도 간의 차이가 그리 크지 않으며, 이는 모델이 데이터를 너무 많이 과적합하거나 과소적합하지 않음을 의미한다.

기계 학습 모델의 편향과 분산을 측정할 수 있는 다른 메트릭들이 있다. 예를 들어 정밀도(precision), 리콜(recall), F1-score, R-squared 등이다. 이러한 메트릭에 대한 자세한 내용과 Python에서 사용하는 방법은 [`sklearn.metrics` 문서](https://scikit-learn.org/stable/modules/model_evaluation.html)에서 확인할 수 있다.

다음 절에서는 모델의 편향과 분산을 줄이고 성능을 개선하는 데 도움이 될 수 있는 기술에 대해 설명한다.

## <a name="sec_05"></a> bias-variance을 줄이는 방법
기계 학습 모델의 편향과 분산을 줄이고 성능을 향상시키는 데 도움을 줄 수 있는 많은 기법들이 있다. 이 절에서는 가장 일반적인 기법과 Python에서 사용하는 방법을 소개할 것이다.

편향과 분산을 모두 줄이는 가장 효과적인 기법 중 하나는 교차 검증(cross-validation)이다. 교차 검증은 데이터를 여러 부분집합으로 분할하고 각 부분집합을 검정 집합으로 사용하면서 나머지 부분집합은 훈련 집합으로 사용하는 방법이다. 이렇게 하면 데이터의 여러 부분에서 모델을 평가하고 성능을 보다 신뢰할 수 있는 추정치를 얻을 수 있다. 교차 검증은 다항식의 차수나 정규화 매개변수와 같은 모델에 가장 적합한 하이퍼파라미터를 선택하는 데 도움이 될 수도 있다. 교차 검증은 `sklearn.model_selection` 모듈의 `cross_val_score` 함수를 사용하여 수행할 수 있다. 예를 들어 주택 가격 데이터세트에서 선형 회귀 모형과 2차 다항식 회귀 모형의 성능을 비교하려고 한다고 가정하자. 10 폴드 교차 검증을 사용하여 각 모델의 평균 MSE를 다음과 같이 계산할 수 있다.

```python
# Import the libraries
import numpy as np
import pandas as pd
from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures


# Load the data
df = pd.read_csv("house_prices.csv")

# Split the data into features and target
X = df.drop("price", axis=1)
y = df["price"]
# Create and fit the linear model
model_linear = LinearRegression()
model_linear.fit(X, y)

# Create and fit the polynomial model
poly = PolynomialFeatures(degree=2)
X_poly = poly.fit_transform(X)
model_poly = LinearRegression()
model_poly.fit(X_poly, y)

# Perform 10-fold cross-validation and calculate the average MSE of each model
mse_linear = -cross_val_score(model_linear, X, y, cv=10, scoring="neg_mean_squared_error").mean()
mse_poly = -cross_val_score(model_poly, X_poly, y, cv=10, scoring="neg_mean_squared_error").mean()
# Print the results
print("Average MSE of linear model:", mse_linear)
print("Average MSE of polynomial model:", mse_poly)
```

위의 코드 출력은 아래와 같다.

```
Average MSE of linear model: 0.046
Average MSE of polynomial model: 0.039
```

보다시피 다항식 모델은 선형 모델보다 평균 MSE가 낮으며, 이는 편향이 낮고 분산이 높다는 것을 의미한다. 이는 다항식 모델이 선형 모델보다 데이터를 더 잘 적합시키고 선형 모델이 데이터를 과소 적합시키고 있음을 시사한다. 그러나 다항식 모델에 대해 너무 높은 차수를 선택함으로써 분산이 증가하고 모델의 일반화 능력이 저하되므로 데이터가 과적합되지 않도록 주의해야 한다.

모델의 편향과 분산을 줄일 수 있는 또 다른 기법으로 정규화를 들 수 있다. 정규화는 모델의 손실함수에 벌칙항을 추가하는 방법으로 모델 모수의 크기를 줄일 수 있다. 이렇게 하면 모델이 너무 복잡해져서 자료가 과적합되는 것을 방지할 수 있다. 정규화는 다중공선성(multicollinearity)을 다루는 데 도움이 될 수도 있다. 이는 일부 특징들이 서로 높은 상관관계를 가지는 상황을 말한다. 정규화에는 L1 정규화(Lasso라고도 함)와 L2 정규화(Ridge라고도 함)가 있다. 정규화는 선형 회귀, 로지스틱 회귀 분석 및 신경망과 같은 다양한 타입의 모델에 적용할 수 있다. 정규화는 `sklearn.linear_model` 모듈에서 `Ridge` 및 `Lasso` 클래스를 사용하여 수행할 수 있다. 예를 들어, 주택 가격 데이터세트에서 선형 회귀 모델과 Ridge 회귀 모델의 성능을 `alpha=0.1`로 비교하려고 한다고 가정하자. 10 폴드 교차 검증을 사용하여 각 모델의 평균 MSE를 다음과 같이 계산할 수 있다.

```python
# Import the libraries
import numpy as np
import pandas as pd
from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LinearRegression, Ridge

# Load the data
df = pd.read_csv("house_prices.csv")

# Split the data into features and target
X = df.drop("price", axis=1)
y = df["price"]

# Create and fit the linear model
model_linear = LinearRegression()
model_linear.fit(X, y)
# Create and fit the ridge model
model_ridge = Ridge(alpha=0.1)
model_ridge.fit(X, y)

# Perform 10-fold cross-validation and calculate the average MSE of each model
mse_linear = -cross_val_score(model_linear, X, y, cv=10, scoring="neg_mean_squared_error").mean()
mse_ridge = -cross_val_score(model_ridge, X, y, cv=10, scoring="neg_mean_squared_error").mean()
# Print the results
print("Average MSE of linear model:", mse_linear)
print("Average MSE of ridge model:", mse_ridge)
```

위의 코드 출력은 아래와 같다.

```
Average MSE of linear model: 0.046
Average MSE of ridge model: 0.045
```

보다시피 ridge 모델은 선형 모델보다 평균 MSE가 약간 낮으며, 이는 편향이 약간 높고 분산이 약간 낮다는 것을 의미한다. 이는 ridge 모델이 데이터의 노이즈와 이상치에 대해 더 강건하고 덜 민감하며 선형 모형이 데이터를 약간 과적합하고 있음을 시사한다. 그러나 ridge 모델에 대해 너무 높은 alpha를 선택하지 않도록 주의해야 하는데, 이는 편향을 증가시키고 모델의 정확성을 떨어뜨릴 수 있기 때문이다.

피처 선택, 피처 공학, 앙상블 방법 등 모델의 편향과 분산을 줄이는 데 도움이 될 수 있는 다른 기법들이 있다. 이 기법들에 대한 자세한 내용과 Python에서 이 기법들을 사용하는 방법을 [sklearn 문서](http://localhost:8888/files/development/github/python_projects/projects/content_bot/%5E13%5E?_xsrf=2%7C5f4d9628%7C92d534f31eaff1c20ab408bb144607f8%7C1704720102)에서 찾을 수 있다. 다음 절에서는 bias-variance 트레이드오프 개념을 다양한 타입의 기계 학습 모델과 문제에 적용할 것이다.

> <span style="color:red">다음 절은 다른 내용임.</span>

## <a name="summary"></a> 마치며
이 포스팅에서 기계 학습에서 bias-variance 트레이드오프에 대해 공부하였다. 편향과 분산이 무엇인지, 기계 학습 모델의 성능에 어떤 영향을 미치는지, 최적의 결과를 얻기 위해 어떻게 균형을 유지하는지 이해했다. 평균 제곱 오차, 정확도, 정밀도, 리콜 및 F1-score와 같은 지표를 사용하여 모형의 편향과 분산을 측정하는 방법도 배웠다. 교차 검증, 정규화, 피처 선택 및 앙상블 방법과 같은 기술을 사용하여 모형의 편향과 분산을 줄이는 방법도 설명하였다. 선형 회귀 분석, 로지스틱 회귀 분석 및 의사 결정 트리와 같은 다양한 타입의 기계 학습 모델에 편의와 분산이 어떻게 영향을 미치는지에 대한 몇 가지 예도 살펴봤다.

> <span style="color:red">의사 결정 트리 예는 없음</span>

> <span style="color:red">코드 실습 미완료</span>
